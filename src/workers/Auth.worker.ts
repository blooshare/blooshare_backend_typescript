import App from "../app/app";

export class AuthWorker {
    public static async getUserAuth(email: string): Promise<any>{
        const result = await App.db.getDbClient().query('select * from public.auth where email=$1', [email]);
        return result.rows || [];
    }

    public static async insertUserAuth(email: string, password: string, role: number): Promise<any>{
        const result = await App.db.getDbClient().query('INSERT INTO public.auth (email, password, role) VALUES ($1, $2, $3)', [email, password, role]);
        return !!result;
    }
}