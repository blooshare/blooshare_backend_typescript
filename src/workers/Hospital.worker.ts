import App from "../app/app";

export class HospitalWorker {
    public static async getAllHospitals(): Promise<any>{
        const result = await App.db.getDbClient().query('select * from public.hospitals');
        
        return result.rows || [];
    }

}