import {Request, Response} from "express";

import {JwtHelper} from "../helpers/Jwt.helper";


export class ExampleController {

    public static async example(req: Request, res: Response) {
        const token = req.header("Authorization");

        if(!token){
            res.status(401).json({message: "no token provided"});
            return;
        }
        if(!JwtHelper.verifyToken(token)){
            res.status(401).json({message: "token not valid"});
            return;
        }

        res.status(200).json({token: token});

    }
}