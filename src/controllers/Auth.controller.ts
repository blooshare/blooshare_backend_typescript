import {Request, Response} from "express";

import * as bcrypt from "bcrypt";


import {AuthWorker} from "../workers/Auth.worker";
import {JwtHelper} from "../helpers/Jwt.helper";
import {AuthModel} from "../models/Auth.model"




export class AuthController {

    public static async login(req: Request, res: Response) {

        console.log("BOD:", req.body);
        const {email, password} = req.body;
        if (email == undefined || password == undefined) {
            res.status(403).json({error: "Missing parameters"});
            return;
        }

        const userQuery = await AuthWorker.getUserAuth(email);


        if (userQuery.length === 0) {
            res.status(404).json({message: "user not found"});
        } else {
            const user = new AuthModel(userQuery[0]);
            if(user.verifyPassword(password)){
                const token = JwtHelper.createJWT(userQuery[0].email, userQuery[0].role);
                res.status(200).json({token: token});
           }else{
                res.status(404).json({message: "password incorrect"});
           }
        }
    }

    public static async signup(req: Request, res: Response) {
        const {email, password} = req.body;
        if (email == undefined || password == undefined) {
            res.status(403).json({error: "Missing parameters"});
            return;
        }

        const user = await AuthWorker.getUserAuth(email);
        if (user.length === 0) {
            const salt = bcrypt.genSaltSync(10);
            const hashPass = bcrypt.hashSync(password, salt);
            const newUser = await AuthWorker.insertUserAuth(email, hashPass, 1);

            if (newUser) {
                const token = JwtHelper.createJWT(email, 1);
                res.status(200).json({token: token});
            } else {
                res.status(404).json({message: "error creating user"});
            }

        } else {
            res.status(400).json({message: "email on use"});
        }


    }

}