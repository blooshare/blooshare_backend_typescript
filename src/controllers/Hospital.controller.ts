import {Request, Response} from "express";

import {JwtHelper} from "../helpers/Jwt.helper";
import {HospitalWorker} from "../workers/Hospital.worker";


export class HospitalController {

    public static async listHospitals(req: Request, res: Response) {
        const token = req.header("Authorization");
        if(!token){
            res.status(401).json({message: "no token provided"});
            return;
        }
        if(!JwtHelper.verifyToken(token)){
            res.status(401).json({message: "token not valid"});
            return;
        }

        const hospitals = await HospitalWorker.getAllHospitals();

        res.status(200).json({hospitals: hospitals});

    }
}