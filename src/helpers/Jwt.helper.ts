import {Request, Response, NextFunction} from "express";
const jwt = require('jsonwebtoken');

export class JwtHelper {
    public static createJWT(email: String, role: number): void {
        const payloadJWT = {
            user: email,
            role: role
        };
        return jwt.sign(payloadJWT, process.env.SECRET_JWT || "SecretPass", {
            expiresIn: process.env.TIME_JWT || 1440 // expires in 24 hours
        });
    }
/*
    public static middlewareJwt (req:  Request, res: Response, next: NextFunction) {
        const token = req.headers['access-token'];

        if (token) {
            jwt.verify(token,process.env.SECRET_JWT || "SecretPass", ((err: any, decoded: any) => {
                if(err){
                    return res.status(401).json({message: "invalid token"});
                }else{
                    next();
                }
            }) )
        }else{
            return res.status(401).json({message: "no token provided"});
        }

    }
*/
    public static verifyToken (token: string): boolean{
        return jwt.verify(token,process.env.SECRET_JWT || "SecretPass", ((err: any) => {
            return !err;
        }) );
    }
}