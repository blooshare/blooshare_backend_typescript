import * as bcrypt from "bcrypt";

export class AuthModel {
    public readonly id: number;
    public readonly email: string;
    public readonly password: string;
    public readonly role: number;

    constructor(data: any){
        this.id = data.id;
        this.email = data.email;
        this.password = data.password;
        this.role = data.role;
    }

    /*
    constructor(id: number, email: string, password: string, role: number) {
        if(data )
        this.id = id;
        this.email = email;
        this.password = password;
        this.role = role;
    }
    */


    verifyPassword (password: string): boolean{
        return bcrypt.compareSync(password, this.password);
    }
}