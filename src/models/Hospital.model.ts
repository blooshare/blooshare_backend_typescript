export class HospitalModel {
    public readonly id: number;
    public readonly name: string;

    constructor(data: any) {
        this.id = data.id;
        this.name = data.name;
    }
}