import {Express} from "express";

import {ExampleController} from '../controllers/Example.controller';

export class ExampleRoute {

    constructor(app: Express){
        this.configureRoutes(app);
    }

    private configureRoutes(app: Express): void{
        app.route("/example").get(ExampleController.example);
    }
}