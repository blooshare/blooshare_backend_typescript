import {Express} from "express";

import {AuthController} from '../controllers/Auth.controller';

export class AuthRoute {

    constructor(app: Express){
        this.configureRoutes(app);
    }

    private configureRoutes(app: Express): void{
        app.route("/auth/login").post(AuthController.login);
        app.route("/auth/signup").post(AuthController.signup);
    }
}