import {Express} from "express";

import {AuthRoute} from './Auth.route';
import {ExampleRoute} from "./Example.route";
import {HospitalRoute} from "./Hospital.route";

export class Routes {

    constructor(app: Express){
        this.configureRoutes(app);
    }

    private configureRoutes(app: Express) {
        app.route("/").get((req, res) => {console.log("¿?"); res.send("Hey! :D")});
        new AuthRoute(app);
        new ExampleRoute(app);
        new HospitalRoute(app);
    }

}