import {Express} from "express";

import {HospitalController} from '../controllers/Hospital.controller';

export class HospitalRoute {

    constructor(app: Express){
        this.configureRoutes(app);
    }

    private configureRoutes(app: Express): void{
        app.route("/hospitals").get(HospitalController.listHospitals);
    }
}