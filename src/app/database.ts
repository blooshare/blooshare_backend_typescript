import {Client} from "pg";

export default class Database {

    private readonly dbClient: Client;

    // private config = {
    //     database: process.env.DATABASE || "NBTech_project",
    //     host: process.env.HOST || "localhost",
    //     password: process.env.PASSWORD || "postgres",
    //     user: process.env.USER || "postgres",
    // };
    private config = {
        database: "NBTech_project",
        host:  "localhost",
        password: "postgres",
        user:  "postgres",
    };
    

    constructor() {

        this.dbClient = new Client(this.config);

        // TODO: Lanzar custom error para reconocer mas rapido
        this.dbClient.connect().then(_ => {
            console.log("DB OK");
        });
    }

    public getDbClient(): Client {
        return this.dbClient;
    }


}