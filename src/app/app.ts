import express, {Express} from "express";
import * as bodyParser from "body-parser";
import cors from "cors";
import timeout from "connect-timeout";
// For access log
import morgan from "morgan";

import {Routes} from '../routes';

import Database from './database';

class App {
    public app: Express;
    public db: Database;

    constructor() {
        this.app = express();
        this.db = new Database();
        this.config();
        this.setup();
    }

    private config(): void {
        this.app.use(bodyParser.urlencoded());
        this.app.use(cors());
        this.app.use(timeout("5000"));
        this.app.use(morgan("dev"));
        this.app.set("secretJWT", process.env.SECRET_JWT || "SecretPass");
        this.app.set("timeJWT", process.env.TIME_JWT || 1440); // 24h
    }

    private setup(): void {
        new Routes(this.app);
    }

}

export default new App();