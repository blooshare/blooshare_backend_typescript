import App from './app/app';

const PORT = 3000;

App.app.listen(PORT, () => {
    console.log("Server start in PORT: " + PORT)
});