FROM node:10

WORKDIR /usr/src/nbtech

COPY . .
RUN npm install && npm run build

EXPOSE 3000
CMD [ "node", "--inspect=5858", "dist/run.js" ]
