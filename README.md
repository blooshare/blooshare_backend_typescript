# Template Node TypeScript API

## Start PG locally with docker
cd docker-database-pg

docker-compose up

## Run project
npm run dev

## Build project
npm run build

## Structure

    dist - Carpeta de JS tras el build. Datos de prod
    src - Archivos TS de la api
        app             Configuracion del server y la base de datos
        controllers     Controladores. Se les invoca desde las routes
        models          Modelos de la base de datos. Se les invoca desde los workers (sobre todo)
        routes          Rutas de la api
        workers         Conexiones a la base de datos
        run.ts          Archivo de lanzamiento del servidor
    docker-compose.yaml - Archivo docker que levanta BBDD, API y PGADMIN
    Dockerfile - Fichero docker de la API
     
     
## TODO:
- Script de lanzamiento de la bbdd en pg
- JWT
    - Crear
    - Desactivar
    - Renovar (Si se implementa solo en node consume memoria. Valorar Redis para implementacion)
- ¿Permitir crear a usuarios temporales?
- Actualizar usuarios
- Borrar / Desactivar usuarios
- Regular accesos
- Limpiar el Package.json (Ordenar dependencies)
